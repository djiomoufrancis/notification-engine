
## Table of Contents

* [Description](#Description)
* [Dependances](#Dependances)
* [Installation](#installation)
* [What's included](#whats-included)
* [Comments](#Comments)
* [Convention de Codage](#Nomenclature)
* [Documentation](#documentation)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [Creators](#creators)
* [Copyright and license](#license)
* [Support IDEL Development](#support-development)


## Description
Le module notification-engine est une extension du service "banq-notification" dans le but d'améliorer le composant sous-jascent. En effet, ce nouveau module possède les caractéristiques suivantes additionnelles par rapport a BAnQ-Notification :
- Déploiement sous forme de micro-service
- Accessibilité a distance (appel de Webservice). Ceci a pour avantage de supprimer la dépendance a ajouter dans chacune des applications qui exploite ce service.
- Possibilité d'attacher des pieces jointes lors de l'envoi d'un mail

Il s'agit d'un programme java 11 développé avec le framework Spring Boot 2.2.2
Le nouveau module exploite la même implémentation de banq-notification (version 1.2), donc, pas de re-écriture de code. Par conséquent utilse la même structure de base de données.


## Dependances

* 💪  [Java 11](https://www.java.com/)
* 💪  [maven-3.6.2](https://maven.apache.org/)
* 💪  [Spring Boot 2.2.2](https://spring.io/projects/spring-boot)
* 💪  [Hibernate 5.3.3](https://hibernate.org/)
* 💪  [Tomcat 9](http://tomcat.apache.org/)
* 💪  [Swagger-UI 2.9.2](https://swagger.io/tools/swagger-ui/)
* 💪  [Lombok 1.18](https://projectlombok.org/)
* 💪  [Oracle 12.1.0](https://oracle.com/)
* 💪  [Banq Notification](http://dev-idel.banq.qc.ca:8081/nexus/repository/maven-group/)
* 💪  [Sonarlint]


## Installation
- Installer Java JDK11
- Installer Maven
- Installer sts (Spring Tool Suite)
- Installer les sources : 
```bash
git clone https://djiomoufrancis@bitbucket.org/banq-dgtit/notification-engine.git
cd notification-engine
mvn -e clean package
```


## What's included

```
idel-refonte-backend/
├── src/
│   ├── main/
│   │   ├── java/
│   │   │   ├── ca.qc.banq.engine.notification/
│   │   │   ├── ca.qc.banq.engine.notification.commun/
│   │   │   ├── ca.qc.banq.engine.notification.business/
│   │   │   ├── ca.qc.banq.engine.notification.dto/
│   │   │   ├── ca.qc.banq.engine.notification.config/
│   │   │   ├── ca.qc.banq.engine.notification.exception/
│   │   │   └── ca.qc.banq.engine.notification.rest/
│   │   ├── resources/
│   │   │   ├── config/
│   │   │   ├── docs/
│   │   │   ├── i18n/
│   │   │   └── application.properties
│   ├── test/
│   │   ├── java/
│   │   │   ├── ca.qc.banq.engine.notification.test.integration/
│   │   ├── resources/
│   │   │   └── application-DEV.properties
├── .gitignore
├── README.md
└── pom.xml
```


## Comments

- Package **ca.qc.banq.engine.notification:** Package de base contenat la classe de demarrage de l'application
- Package **ca.qc.banq.engine.notification.commun:** Contient les objets partages par tous les composants de l'application
- Package **ca.qc.banq.engine.notification.config:** Contient les differentes configurations
- Package **ca.qc.banq.engine.notification.exception:** Contient les Exceptions
- Package **ca.qc.banq.engine.notification.business:** Couche des traitements métier
- Package **ca.qc.banq.engine.notification.dto:** Couche des modèles de données
- Package **ca.qc.banq.engine.notification.rest:** Couche des webservices rest
- ressource **config** Contient les differents profils de configuration
- ressource **docs** Contient les ressources representant la documentation des API et la documentation des sources du projet
- ressource **i18n** Contient les fichiers de messages utilises pour l'internationalisation
- ressource **application.properties** Fichier de configuration de base de l'application  

**Differentes configurations de l'application**:  
		+ *AppConfig.java* : Configuration globale de l'application  
		+ *DaoConfig.java* : Configuration de la couche d'acces aux donnees  
		+ *SwaggerConfig.java* : Configuration de la documentation Swagger  
		+ *WebMvcConfig.java* : Configuration MVC  
		+ *SecurityConfig.java* : Configuration de la securite d'acces a l'application  

```
| No | Package     | Description                                            |
|----|:-----------:|-------------------------------------------------------:|
|  1 | commun      | utilitaires partagés                                   |
|  2 | config      | configurations du module                               |
|  3 | exception   | Les Exceptions                                         |
|  4 | dto         | Les Modeles de données                                 |
|  5 | business    | Les traitements métier                                 |
|  6 | rest        | Webservices Rest                                       |
```

## Convention de Codage

- Toutes interfaces débutent par un «**I**» (i majuscule)
- Toutes Enumérations débutent par un «**E**» (e majuscule)
- Les classes utilisent la convention de nommage standard dite de «**Camel Case**» (premiere letre de chaque mot en majuscule)
- Une classe qui implémente une interface se termine par «**Impl**»
- Les classes de type controlleurs se terminent par le mot «**Controller**»
- Les classes de type Repository se terminent par le mot «**Repository**»
- Les interfaces de services métier se terminent par le mot «**Service**»
- Les classes de configuration se terminent par le mot «**Config**»


## Documentation
#### API & Javadoc
The documentation for the notification-engine is hosted at :
- » API Documentation : <https://lab-idel.banq.qc.ca:8680/notification-engine/apidoc/index.html>
- » Javadoc : <https://lab-idel.banq.qc.ca:8680/notification-engine/javadoc/index.html>



##### Architecture
![Architecture](src/main/resources/docs/archicode.png)  


	
## Procédure de déploiement notification-engine :
 
#### Pré-requis
 
* Disposer d'un Serveur Linux (RedHat ou Debian)
* Avoir les ressources suivantes :
	- Fichier notification-engine.jar (généré dans target après le build du projet)
	- Répertoire config (disponible dans src/main/resources/)
* Avoir installé Java sur le serveur (ex: alternative par défaut sur /usr/bin/java)
 
#### Copier les ressources sur le serveur
- Accéder au serveur	
- Créer le *WorkingDirectory* de l'application notification-engine (ex: /opt/notification)

```bash
  sudo mkdir /opt/notification
```
- Copier les ressources a déployer (notification-engine.jar et répertoire config) dans le *WorkingDirectory*

```
  cp notification-engine.jar /opt/notification/
  cp -r config /opt/notification/
```

#### Créer le service notification-engine.service  
Ceci est nécessaire au premier déploiement et permet de démarrer les service automatiquement par l'OS au démarrage du système

```
vim /usr/lib/systemd/system/notification-engine.service
```

- ajouter le contenu suivant au fichier

```
[Unit]
Description=BANQ Notification Service
Requires=network.service
After=network.service

[Service]
Restart=on-failure

WorkingDirectory=/opt/notification
ExecStart=/usr/bin/java -jar /opt/notification/notification-engine.jar
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
```

- Sauvegarder et fermer le fichier

```
[Echap]
:wq!
[Enter]
```
- Activer le service

```
sudo systemctl enable notification-engine.service
```
 
#### Choisir l'environnement d'exécution
- Editer le fichier de configuration application.properties

```
vim /opt/notification/config/application.properties
```
 
- Modifier le profil actif en choisisant l'environnement a executer (les valeurs possibles sont: DEV, ACT ou PROD)
DEV = Environnement de développement 
ACT = Environnement d'acceptation (ou tests)
PROD = Environnement de Production

```
spring.profiles.active=DEV
```
- Sauvegarder et fermer le fichier

```
[Echap]
:wq!
[Enter]
```
Redémarrer le service  *notification-engine* pour appliquer les modifications
 
#### Démarrer le service
```
sudo systemctl start notification-engine
```
 
#### Redémarrer le service
Cette action sera généralement indispensab;e en cas de mise a jour de l'application (ex: modification d'un fichier de configuration ou déploiement d'une nouvelle version du fichier jar).
Executer la commande suivante :
```
sudo systemctl restart notification-engine
```
 
#### Arrêter le service
```
sudo systemctl stop notification-engine
```
 
#### Vérifier le statut du service
```
sudo systemctl status notification-engine
```
 
#### Consulter en temps réel les logs du service
```
tail -lf /opt/notification/logs/notification-engine.log
```



## Contributing

Please read through our [contributing guidelines] opening issues, coding standards, and notes on development.
http://jira.banq.qc.ca/jira/secure/Dashboard.jspa


## Versioning

For transparency into our release cycle and in striving to maintain backward compatibility, notification-engine is maintained under http://dev-idel.banq.qc.ca:8081/nexus/repository/maven-group/


## Creators

** BANQ **

* <http://www.banq.qc.ca/accueil/>


## Copyright and license

copyright 2020 BANQ.


## Support IDEL Development

notification-engine is an BANQ licensed open source project and completely free to use. However, the amount of effort needed to maintain and develop new features for the project is not sustainable without proper financial backing.
