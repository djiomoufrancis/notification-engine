/**
 * 
 */
package ca.qc.banq.engine.notification.rest;

import ca.qc.banq.engine.notification.dto.NotificationDto;

/**
 * Interface des services de notification
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public interface INotificationWebservice {


	/**
	 * Envoi une notification
	 * @param notification notification a envoyer
	 * @return true/false
	 */
	public void sendNotification(NotificationDto notification);

}

