/**
 * 
 */
package ca.qc.banq.engine.notification.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Notification Data
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0 
 * @since 2019-10-21
 */
@SuppressWarnings("serial")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDto implements Serializable {

	@ApiModelProperty(value = "Application or User which uses this Call")
	private String userID;
	
	@NotEmpty(message = "NotificationDto.object.CannotBeEmpty")
	@ApiModelProperty(value = "Object of the message", example = "(none)", allowEmptyValue = false)
	private String object;
	
	@NotEmpty
	@ApiModelProperty(value = "Body (text plain content) of the message", example = "(none)")
	private String body;
	
	@NotEmpty(message = "NotificationDto.members.CannotBeEmpty") @NotNull(message = "NotificationDto.members.CannotBeNull")
	@ApiModelProperty(value = "List of senders and/or receivers of the message", allowEmptyValue = false)
	private NotificationMembersDto[] members;
	
	@ApiModelProperty(value = "Configuration d'envoi d'Email")
	private MailConfigDto emailConfig;
	
}

