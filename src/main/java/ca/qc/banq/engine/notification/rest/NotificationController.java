package ca.qc.banq.engine.notification.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.qc.banq.engine.notification.business.INotificationBusiness;
import ca.qc.banq.engine.notification.dto.NotificationDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Implementation des webservices communs
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@RestController
@RequestMapping("/api/common")
@Api(description = "Service de Notification")
public class NotificationController implements INotificationWebservice {

	@Autowired
	INotificationBusiness notificationService;
	
	/*
	 * (non-Javadoc)
	 * @see ca.qc.banq.idel.extranet.servicerest.commun.ICommonWebservice#sendNotification(ca.qc.banq.idel.extranet.dto.commun.NotificationDto)
	 */
	@Override
	@PostMapping("/envoyerNotification")
	@ApiOperation("Service Generique de notification")
	public void sendNotification(@RequestBody @ApiParam(value = "Notification a envoyer") @Valid NotificationDto notification) {

		// Send Notification
		notificationService.saveNotification(notification);
	
	}

}

