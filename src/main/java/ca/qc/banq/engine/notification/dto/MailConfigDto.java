/**
 * 
 */
package ca.qc.banq.engine.notification.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Configurations avancees d'un mail a envoyer
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 *
 */
@SuppressWarnings("serial")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MailConfigDto implements Serializable {
	
	@ApiModelProperty(value = "Envoyer un mail (oui/non)")
	private boolean sendEmail = false;
	
	@ApiModelProperty(value = "Nom personalise de l'expediteur")
	private String fromName;
	
	@ApiModelProperty(value = "Contenu du message au format html")
	private String htmlText;
	
	@ApiModelProperty(value = "Adresse de reponse")
	private String replyTo;

	@ApiModelProperty(value = "Liste des pieces jointes (each file is Base64Encoded as String)")
	private AttachmentDto[] attachmentFiles;

}

