/**
 * 
 */
package ca.qc.banq.engine.notification.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ca.qc.banq.engine.notification.commun.MessagingComponent;
import lombok.extern.slf4j.Slf4j;

/**
 * Intercepteur des exceptions levees par les webservices ( controlleurs )
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler  {
	
	@Autowired
	MessagingComponent messaging;
	
	/**
	 * interception de tout autre type d'exception
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = { Exception.class, NotificationException.class, ConstraintViolationException.class })
	protected ResponseEntity<List<String>> handleConflict(Exception ex, WebRequest request) {
		
		// initialisation de la liste des erreurs a retourner
		List<String> erreurs = new ArrayList<String>();
		
		// Calcul du statut et du message d'erreur a retourner
		HttpStatus status = (ex instanceof NotificationException ) ? HttpStatus.BAD_REQUEST : (ex instanceof AccessDeniedException || ex instanceof AuthorizationServiceException ? HttpStatus.UNAUTHORIZED : HttpStatus.INTERNAL_SERVER_ERROR) ;
		erreurs.add( ex.getMessage() );
		
        // Verification s'il s'agit d'une exception liee a une violation de contrainte d'integrite
		if(ExceptionUtils.getRootCause(ex) instanceof ConstraintViolationException) {
			ConstraintViolationException ex2 = (ConstraintViolationException) ExceptionUtils.getRootCause(ex);
			// recuperation de la liste des contraintes violees
			erreurs.addAll( ex2.getConstraintViolations().stream().map(x -> messaging.translate(x.getMessage())).collect(Collectors.toList()) );
			// MAJ du type et du message d'erreur a retourner
			status = HttpStatus.BAD_REQUEST;
		}
		
		// Log
        log.error(ex.getMessage(), ex);
        return ResponseEntity.status(status).body(erreurs);
	}
	
	/**
	 * Interception des exceptions de validation des donnees
	 */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException excetion, HttpHeaders headers, HttpStatus statut, WebRequest request) {
        List<String> erreurs = excetion.getBindingResult().getFieldErrors().stream().map(x -> messaging.translate(x.getDefaultMessage()) ).collect(Collectors.toList());
        return ResponseEntity.badRequest().body(erreurs );
    }		
	
}
