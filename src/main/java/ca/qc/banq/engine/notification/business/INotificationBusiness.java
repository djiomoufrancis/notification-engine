/**
 * 
 */
package ca.qc.banq.engine.notification.business;

import java.util.List;

import org.springframework.stereotype.Service;

import ca.qc.banq.engine.notification.dto.NotificationDto;
import ca.qc.banq.notification.entity.Notification;

/**
 * Interface metier de gestion des notifications
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public interface INotificationBusiness {

	public void saveNotification(NotificationDto notification);
	
	public Notification findNotification(Long id);
	
	public List<Notification> listNotifications();
	
}

