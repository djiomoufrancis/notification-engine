package ca.qc.banq.engine.notification.commun;

/**
 * Notification Member Type
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
public enum ENotificationMemberType {
	
	CC("ReceiverType.Copy"),
	CCI("ReceiverType.Hidden"),
	DES("ReceiverType.Receiver"),
	EXP("ReceiverType.Sender");
	
	/**
	 * Valeur
	 */
	private String value;
	
	/**
	 * Constructeur
	 * @param value
	 */
	private ENotificationMemberType(String value){
		this.setValue(value);
	}
	

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}
