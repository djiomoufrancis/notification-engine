/**
 * 
 */
package ca.qc.banq.engine.notification.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import ca.qc.banq.engine.notification.commun.ENotificationMemberType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@SuppressWarnings("serial")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class NotificationMembersDto implements Serializable {
	
	@NotEmpty
	@ApiModelProperty(value = "Email address of the member", example = "name@domain.ca", allowEmptyValue = false)
	private String emailAddress;
	
	@NotEmpty
	@ApiModelProperty(value = "Member Type (can be: EXP, DES, CC or CCI)", example = "DES", allowEmptyValue = false)
	private ENotificationMemberType type;
	
}

