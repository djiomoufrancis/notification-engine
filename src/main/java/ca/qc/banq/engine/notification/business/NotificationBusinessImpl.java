/**
 * 
 */
package ca.qc.banq.engine.notification.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.qc.banq.engine.notification.commun.MailService;
import ca.qc.banq.engine.notification.dto.NotificationDto;
import ca.qc.banq.engine.notification.dto.NotificationMembersDto;
import ca.qc.banq.notification.domaine.TypeDestinataire;
import ca.qc.banq.notification.domaine.TypeNotification;
import ca.qc.banq.notification.entity.Notification;
import ca.qc.banq.notification.entity.NotificationDest;
import ca.qc.banq.notification.servicemetier.NotificationClientServiceMetier;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation du service metier de gestion des notifications
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
@Slf4j
public class NotificationBusinessImpl implements INotificationBusiness {

	/**
	 * Inject notification Service
	 */
	@Autowired
	private NotificationClientServiceMetier notificationService;
	
	@Autowired
	MailService mailService;
	
	/* (non-javadoc)
	 * @see ca.qc.banq.engine.notification.business.INotificationBusiness#saveNotification(ca.qc.banq.notification.entity.Notification)
	 */
	@Override
	public void saveNotification(NotificationDto notification) {

		// Create notification
		Notification msg = new Notification();
		msg.setCodeModeleDocument("1");
		msg.setCodeReference("1");
		msg.setContenuBrut(notification.getBody());
		msg.setObjet(notification.getObject());
		msg.setContenuHtml(notification.getEmailConfig() != null ? notification.getEmailConfig().getHtmlText() : "");
		msg.setDateCreation(new Date());
		msg.setDateEnvoi(new Date());
		msg.setDateModification(null);
		msg.setTypeNotification(TypeNotification.MES);
		msg.setUserCreation(notification.getUserID());
		
		// Create the list of receivers
		List<NotificationDest> dests = new ArrayList<NotificationDest>();
		for(NotificationMembersDto m : notification.getMembers()) {
			NotificationDest dest = new NotificationDest();
			dest.setCourriel(m.getEmailAddress());
			dest.setTypeDestinataire(TypeDestinataire.valueOf(m.getType().toString()));
			dest.setIndLu(false);
			dest.setNotification(msg);
			dests.add(dest);
		}
		msg.setNotificationsDest(dests);
		
		try {
			if(notification.getEmailConfig() != null && notification.getEmailConfig().isSendEmail()) mailService.sendEmail(notification);
			msg.setDateEnvoi(new Date());
			msg.setIndSucces(true);
		} catch(Exception ex) {
			log.error(ex.getMessage(), ex);
			msg.setIndSucces(false);
		}

		// Save Notification
		notificationService.enregistrerNotification(msg);
		
	}

	/* (non-javadoc)
	 * @see ca.qc.banq.engine.notification.business.INotificationBusiness#findNotification(java.lang.Long)
	 */
	@Override
	public Notification findNotification(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-javadoc)
	 * @see ca.qc.banq.engine.notification.business.INotificationBusiness#listNotifications()
	 */
	@Override
	public List<Notification> listNotifications() {
		// TODO Auto-generated method stub
		return null;
	}

}

