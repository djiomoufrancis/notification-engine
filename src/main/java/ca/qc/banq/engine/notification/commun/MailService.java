/**
 * 
 */
package ca.qc.banq.engine.notification.commun;

import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;

import ca.qc.banq.engine.notification.config.AppConfig;
import ca.qc.banq.engine.notification.dto.AttachmentDto;
import ca.qc.banq.engine.notification.dto.NotificationDto;
import ca.qc.banq.engine.notification.dto.NotificationMembersDto;
import ca.qc.banq.engine.notification.exception.NotificationException;

/**
 * Service d'envoi de mails
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Service
public class MailService {

	/**
	 * Inject and initialize an Email Sender
	 */
	@Autowired
    public JavaMailSender emailSender;
	
	/**
	 * Temporary directory (requested for files attachments)
	 */
	private String tmpDir = "tmp";
	private String tmpPath = AppConfig.workPathDir.concat(tmpDir).concat(File.separator);
	
	@Value("${notification.default.fromname}")
	private String FROMNAME;

	@Value("${spring.mail.username}")
	private String FROM;
	
	@Value("${spring.mail.default-encoding}")
	private String encoding;

	/**
	 * 
	 * @param from Sender email address
	 * @param fromName Sender Name
	 * @param replyTo Reply to email address
	 * @param to Receivers
	 * @param subject Subject
	 * @param text Body of the mail (in text plain format)
	 * @param htmlText body of the mail (in html format)
	 * @param copies copies to (other receivers)
	 * @param attachments List of Attachment files
	 * @throws Exception
	 */
    private void sendEmail(String from, String fromName, String replyTo, List<String> to, String subject, String text, String htmlText, List<String> copies, List<String> hidden, List<File> attachments) throws Exception {
    	
    	// Initialize a MimeMessage
    	MimeMessage message = emailSender.createMimeMessage();
    	//message.setSubject(subject, encoding);
    	//message.setContent(!Strings.isNullOrEmpty(htmlText) ? htmlText : text, !Strings.isNullOrEmpty(htmlText) ? "text/html; charset=ISO-8859-1" : "text/plain; charset=UTF-8");
    	
    	MimeMessageHelper helper = new MimeMessageHelper(message, true, encoding );
    	
    	// Set Texts
    	helper.setSubject(subject);
    	helper.setText(!text.equalsIgnoreCase(htmlText) ? text : "", htmlText);
    	helper.setFrom( !Strings.isNullOrEmpty(from) ? from : FROM, fromName );
    	if( !Strings.isNullOrEmpty(replyTo)) helper.setReplyTo(replyTo);
    	
    	// Add Receivers
    	if(to != null && !to.isEmpty()) to.forEach(t -> { try { helper.addTo(t); } catch (MessagingException e) {} }  );
        
        // Add Copies
    	if(copies != null && !copies.isEmpty()) copies.forEach(cc -> { try { helper.addCc(cc); } catch (MessagingException e) {} } );

        // Add Copies Cachees
    	if(hidden != null && !hidden.isEmpty()) hidden.forEach(bcc -> { try { helper.addBcc(bcc); } catch (MessagingException e) {} } );
        
        // Add Attachments
        if(attachments != null && !attachments.isEmpty()) attachments.forEach(f -> { try { helper.addAttachment(f.getName(), f); } catch (MessagingException e) {} } );
        
        // Send Message
        emailSender.send(message);
    }
    
    /**
     * Send Notifiaction by Mail
     * @param dto Notification
     * @throws All Exceptions
     */
    public void sendEmail(NotificationDto dto) throws Exception {
    	
    	// Validate object
    	if(dto == null) throw new NotificationException("Invalid.NotificationObject.Exception");
    	if(dto.getEmailConfig() == null) throw new NotificationException("Notification.EmailConfig.NotNull");
    	if(!dto.getEmailConfig().isSendEmail()) throw new NotificationException("Notification.EmailConfig.SendEmail.isFalse");
    	if(dto.getMembers() == null || dto.getMembers().length == 0) throw new NotificationException("Notification.Members.isEmpty");
    	
    	// Initialisations
    	List<String> to = new ArrayList<String>();
    	List<String> cc = new ArrayList<String>();
    	List<String> cci = new ArrayList<String>();
    	List<File> files = new ArrayList<File>();
    	String from = null;
    	
    	// Build Members
    	for(NotificationMembersDto member : dto.getMembers()) {
    		switch( member.getType() ) {
	    		case EXP: from = member.getEmailAddress(); break;
	    		case DES: to.add(member.getEmailAddress()); break;
	    		case CC: cc.add(member.getEmailAddress()); break;
	    		case CCI: cci.add(member.getEmailAddress()); break;
    		}
    	} 
    	
    	// Attach Files
    	if(dto.getEmailConfig().getAttachmentFiles() != null && dto.getEmailConfig().getAttachmentFiles().length > 0) {
    		if(!new File(tmpPath).exists()) new File(tmpPath).mkdirs();
    		for(AttachmentDto f : dto.getEmailConfig().getAttachmentFiles()) {
    			if(f != null) {
    				File file = new File(tmpPath.concat(f.getFileName()));
    				FileUtils.writeByteArrayToFile(file, Base64.getDecoder().decode(f.getBase64EncodedFile()));
    				files.add(file);
    			}
    		}
    	}
    	
    	// Send Email
    	sendEmail(from, dto.getEmailConfig().getFromName(), dto.getEmailConfig().getReplyTo(), to, dto.getObject(), dto.getBody() != null ? dto.getBody() : "" , dto.getEmailConfig().getHtmlText() != null ? dto.getEmailConfig().getHtmlText() : "", cc, cci, files  );
    	
    	// Clean Temp Directory
    	FileUtils.deleteQuietly(new File (tmpPath));
    }
    
}
