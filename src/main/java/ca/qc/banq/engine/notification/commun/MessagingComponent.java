package ca.qc.banq.engine.notification.commun;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Composant de gestion des messages
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@Slf4j
@Component
public class MessagingComponent {

	@Autowired
	MessageSource messageSource;
	
	/**
	 * Obtient de la traduction d'un code de message (internationalization) 
	 * @param code Code de message
	 * @param params Liste des parametres
	 * @return Message traduit
	 */
	public String translate(String code, String... params) {
		String msg = code;
		try {
			msg = messageSource.getMessage(code, params, LocaleContextHolder.getLocale());
		} catch(Exception ex) {
			// Log
			log.error(ex.getMessage());
		}
		return msg;
	}
}
