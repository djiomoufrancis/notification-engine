package ca.qc.banq.engine.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import ca.qc.banq.engine.notification.config.AppConfig;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Classe de Demarrage
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 */
@EnableAsync
@EnableSwagger2
@EnableScheduling
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = {"ca.qc.banq.engine.notification", "ca.qc.banq.notification"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

}

