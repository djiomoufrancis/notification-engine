/**
 * 
 */
package ca.qc.banq.engine.notification.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Configurations avancees d'un mail a envoyer
 * @author <a href="mailto:francis.djiomou@banq.qc.ca">Francis DJIOMOU (Project Engineer and Research)</a>
 * @version 1.0
 * @since 2019-10-21
 *
 */
@SuppressWarnings("serial")
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AttachmentDto implements Serializable {

	@ApiModelProperty(value = "nom du fichier")
	private String fileName;
	
	@ApiModelProperty(value = "Contenu du fichier encode en base64")
	private String base64EncodedFile;
	
}

